class CreateQuestions < ActiveRecord::Migration[5.0]
  def change
    create_table :questions do |t|
      t.belongs_to :sub_topic, index: true
      t.belongs_to :test, index: true
      t.text :description, null: false
      t.string :image
      t.integer :answer_id
      t.timestamps
    end
  end
end
