class CreateOptions < ActiveRecord::Migration[5.0]
  def change
    create_table :options do |t|
    	t.belongs_to :question, index: true
    	t.text :description, null: false
    	t.string :image
    	t.string :solution_image

      t.timestamps
    end
  end
end
