class CreateSubTopics < ActiveRecord::Migration[5.0]
  def change
    create_table :sub_topics do |t|
      t.belongs_to :topic, index: true
      t.string :name

      t.timestamps
    end
  end
end
