# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Admin.create(email: "pavank.iitkgp@gmail.com", password: "pavan123", password_confirmation: "pavan123")
Topic.create(name: "Physics")
Topic.create(name: "Maths")
Topic.create(name: "Chemistry")
s = SubTopic.new(name: "General Organic Chemistry")
s.topic = Topic.find_by_name ("Chemistry")
s.save
