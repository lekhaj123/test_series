class SubTopic < ApplicationRecord
  has_many :questions
  belongs_to :topic
end
