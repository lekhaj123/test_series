# All end-user controllers should inherit from this controller
class AdminController < ApplicationController
  before_filter :authenticate_admin!
end