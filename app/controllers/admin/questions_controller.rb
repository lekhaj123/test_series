class Admin::QuestionsController < AdminController
  before_filter :authenticate_admin!

  def index
    @questions = Question.all
  end

  def create
    params.inspect
    # @option1 = Option.create!()
    # @question = Question.create!(params[:question])
    # Save Images to directory or s3 and populate them on tests.
    question = Question.create!(:description => params[:question][:description],
      :sub_topic => SubTopic.find(params[:question][:sub_topic].to_i))
    correct_option = Option.new(:description => params[:option1], 
      :solution_image => (params[:option1_solution].nil? ? "No Image" : params[:option1_solution].original_filename))
    option2 = Option.new(:description => params[:option2], 
      :solution_image => (params[:option2_solution].nil? ? "No Image" : params[:option2_solution].original_filename))
    option3 = Option.new(:description => params[:option3], 
      :solution_image => (params[:option3_solution].nil? ? "No Image" : params[:option3_solution].original_filename))
    option4 = Option.new(:description => params[:option4], 
      :solution_image => (params[:option4_solution].nil? ? "No Image" : params[:option4_solution].original_filename))
    question.inspect
    correct_option.question = question
    correct_option.save
    option2.question = question
    option2.save
    option3.question = question
    option3.save
    option4.question = question
    option4.save
    question.answer_id = correct_option.id
    question.save
  end

  def new
    @question = Question.new
  end

  def edit

  end

  def show

  end

  def update

  end

  def destroy
    params.inspect
    Question.find(params[:id]).destroy
  end
end
