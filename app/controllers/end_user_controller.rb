# All end-user controllers should inherit from this controller
class EndUserController < ApplicationController
  before_filter :authenticate_user!
end