CKEDITOR.editorConfig = function( config )
{
config.extraPlugins = 'widget';
config.extraPlugins = 'lineutils';
config.extraPlugins = 'mathjax';
config.extraPlugins = 'eqneditor';

  config.toolbar_mini = [
    ["Bold",  "Italic",  "Underline",  "Strike",  "-",  "Subscript",  "Superscript", "EqnEditor"],
  ];
  config.toolbar = "simple";
};